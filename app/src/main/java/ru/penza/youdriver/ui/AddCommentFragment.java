package ru.penza.youdriver.ui;

import android.app.DialogFragment;
import android.app.LoaderManager;
import android.content.DialogInterface;
import android.content.Loader;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ru.penza.youdriver.R;
import ru.penza.youdriver.loaders.AddCommentLoader;

public class AddCommentFragment extends DialogFragment implements View.OnClickListener, LoaderManager.LoaderCallbacks<Boolean> {

    public static final String TAG = "AddCommentFragment";
    private static final String KEY_STATE_NUMBER = "StateNumber";
    private static final String KEY_REGION = "Region";
    public static final int EMPTY = 0;

    private EditText stateNumberEdit;
    private EditText regionEdit;
    private EditText commentEdit;
    private String strStateNumber;
    private String strRegion;
    private Listener listener;

    interface Listener {
        public void resetUI(DialogInterface dialog);
    }

    public static AddCommentFragment getInstance() {
        return new AddCommentFragment();
    }

    public static AddCommentFragment getInstance(Listener listener, String stateNumber, String region) {
        AddCommentFragment fragment = new AddCommentFragment();
        Bundle args = new Bundle();
        args.putString(KEY_STATE_NUMBER, stateNumber);
        args.putString(KEY_REGION, region);
        fragment.setArguments(args);
        fragment.setListener(listener);
        return fragment;
    }

    private void setListener(Listener listener) {
        this.listener = listener;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            if (args.containsKey(KEY_STATE_NUMBER)) {
                strStateNumber = args.getString(KEY_STATE_NUMBER);
            }
            if (args.containsKey(KEY_REGION)) {
                strRegion = args.getString(KEY_REGION);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(R.string.title_add_comment);
        View root =  inflater.inflate(R.layout.dialog_add_comment, container, false);
        stateNumberEdit = (EditText) root.findViewById(R.id.edt_state_number);
        regionEdit = (EditText) root.findViewById(R.id.edt_region);
        commentEdit = (EditText) root.findViewById(R.id.edt_comment);
        ((Button) root.findViewById(R.id.btn_poison)).setOnClickListener(this);
        resetUI();
        return root;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_poison:
                if (isValidate()) {
                    getLoaderManager().initLoader(1, null, this);
                    listener.resetUI(getDialog());
                }
                break;
            default:
                break;
        }
    }

    private boolean isValidate() {
        boolean flag = true;
        if (stateNumberEdit.getText().length() == EMPTY) {
            stateNumberEdit.setError(getString(R.string.err_state_number_empty));
            flag = false;
        }
        if (regionEdit.getText().length() == EMPTY) {
            regionEdit.setError(getString(R.string.err_region_empty));
            flag = false;
        }
        if (commentEdit.getText().length() == EMPTY) {
            commentEdit.setError(getString(R.string.err_state_comment_empty));
            flag = false;
        }
        return flag;
    }

    private void resetUI() {
        if (strStateNumber != null && strRegion != null ) {
            stateNumberEdit.setText(strStateNumber);
            regionEdit.setText(strRegion);
            stateNumberEdit.setEnabled(false);
            regionEdit.setEnabled(false);
        } else {
            stateNumberEdit.setEnabled(true);
            regionEdit.setEnabled(true);
        }
    }

    @Override
    public Loader<Boolean> onCreateLoader(int id, Bundle bundle) {

        Loader loader = null;
        if (id == 1) {

            loader = new AddCommentLoader(getActivity(),
                    stateNumberEdit.getText().toString() ,
                    regionEdit.getText().toString(),
                    commentEdit.getText().toString()
            );
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Boolean> integerLoader, Boolean status) {
        if (status == null) {
            Toast.makeText(getActivity(), R.string.toast_not_internet, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onLoaderReset(Loader<Boolean> integerLoader) {}
}
