package ru.penza.youdriver.ui;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import ru.penza.youdriver.R;
import ru.penza.youdriver.entities.Comment;

public class CommentsAdapter extends BaseAdapter {

    public static final String KEY_EDIT_KARMA = "karma";
    public static final int KEY_MSG_ADAPTER = 1;
    public static final String KEY_POSITIVE_KARMA = "positive";
    public static final String KEY_NEGATIVE_KARMA = "negative";
    public static final String KEY_POSITION = "position";

    private Context context;
    private LayoutInflater lInflater;
    private Handler handler;
    private List<Comment> data;

    //TODO Изменить
    public CommentsAdapter(Context context, final Handler handler, List<Comment> data) {
        this.context = context;
        this.lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.handler = handler;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Comment getItem(int id) {
        //TODO ИЗМЕНИТЬ
        return data.get(id);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public View getView(final int id, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = lInflater.inflate(R.layout.adapter_comments, viewGroup, false);
        }

        String strDate = null;
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ").parse(getItem(id).getDate());
            strDate = new SimpleDateFormat("dd.MM.yyyy").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        ((TextView) view.findViewById(R.id.txt_date)).setText(strDate);
        ((TextView) view.findViewById(R.id.txt_comment)).setText(getItem(id).getContent());
        ((TextView) view.findViewById(R.id.txt_karma)).setText(context.getString(R.string.txt_karma, getItem(id).getKarma()));

        ((ImageView) view.findViewById(R.id.img_positive)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                poisonMSG(KEY_POSITIVE_KARMA, id);
            }
        });

        ((ImageView) view.findViewById(R.id.img_negative)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                poisonMSG(KEY_NEGATIVE_KARMA, id);
            }
        });

        return view;
    }

    public void poisonMSG (String str, int id) {
        Message message = handler.obtainMessage();
        Bundle data = new Bundle();
        data.putString(KEY_EDIT_KARMA, str);
        data.putInt(KEY_POSITION, id);
        message.what = KEY_MSG_ADAPTER;
        message.setData(data);
        handler.sendMessage(message);
    }
}
