package ru.penza.youdriver.ui;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import ru.penza.youdriver.R;

public class ChoiceLicencePlateFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = "ChoiceLicencePlateFragment";
    public static final String KEY_STATE_NUMBER = "stateNumber";
    public static final String KEY_REGION = "region";
    private static final int EMPTY = 0;

    private EditText stateNumberEdit;
    private EditText regionEdit;

    public static ChoiceLicencePlateFragment getInstance() {
        return new ChoiceLicencePlateFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_choice_licence_plate, container, false);
        stateNumberEdit = (EditText) root.findViewById(R.id.edt_state_number);
        regionEdit = (EditText) root.findViewById(R.id.edt_region);
        ((Button) root.findViewById(R.id.btn_choice)).setOnClickListener(this);
        ((Button) root.findViewById(R.id.btn_reset)).setOnClickListener(this);
        return root;
    }

    private boolean isValidate() {
        boolean flag = true;
        if (stateNumberEdit.getText().length() == EMPTY) {
            stateNumberEdit.setError(getString(R.string.err_state_number_empty));
            flag = false;
        }
        if (regionEdit.getText().length() == EMPTY) {
            regionEdit.setError(getString(R.string.err_region_empty));
            flag = false;
        }
        return flag;
    }

    private void onCreateDriver() {
        Intent intent = new Intent(getActivity(), LicencePlateActivity.class);
        intent.putExtra(KEY_STATE_NUMBER, stateNumberEdit.getText().toString());
        intent.putExtra(KEY_REGION, regionEdit.getText().toString());
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_choice:
                if (isValidate()) {
                    onCreateDriver();
                }
                break;
            case R.id.btn_reset:
                stateNumberEdit.setText("");
                regionEdit.setText("");
                break;
            default:
                break;
        }
    }
}
