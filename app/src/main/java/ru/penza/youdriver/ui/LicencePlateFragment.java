package ru.penza.youdriver.ui;


import android.app.DialogFragment;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.DialogInterface;
import android.content.Loader;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.concurrent.ExecutionException;

import ru.penza.youdriver.R;
import ru.penza.youdriver.entities.Comment;
import ru.penza.youdriver.entities.LicencePlate;
import ru.penza.youdriver.loaders.ReadLicencePlateLoader;
import ru.penza.youdriver.loaders.UpdateCommentKarmaTask;

public class LicencePlateFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<LicencePlate>> {

    public static final String TAG = "LicencePlateFragment";
    private static final int LOADER_LICENCE_PLATE = 1;

    private String strStateNumber;
    private String strRegion;
    private LicencePlate data = new LicencePlate();
    private TextView stateNumber;
    private ListView commentsList;
    private ProgressBar loadCommentsPRB;
    private TextView emptyComments;
    private TextView karma;
    private CommentsAdapter adapter;

    public static LicencePlateFragment getInstance() {
        return new LicencePlateFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity().getIntent() != null &&
                getActivity().getIntent().getStringExtra(ChoiceLicencePlateFragment.KEY_STATE_NUMBER) != null &&
                getActivity().getIntent().getStringExtra(ChoiceLicencePlateFragment.KEY_REGION) != null) {
            strStateNumber = getActivity().getIntent().getStringExtra(ChoiceLicencePlateFragment.KEY_STATE_NUMBER);
            strRegion = getActivity().getIntent().getStringExtra(ChoiceLicencePlateFragment.KEY_REGION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root =  inflater.inflate(R.layout.fragment_licence_plate, container, false);
        stateNumber = (TextView) root.findViewById(R.id.txt_state_number);
        commentsList = (ListView) root.findViewById(R.id.list_comments);
        loadCommentsPRB = (ProgressBar) root.findViewById(R.id.prb_load_comments);
        emptyComments = (TextView) root.findViewById(R.id.txt_empty_comments);
        karma = (TextView) root.findViewById(R.id.txt_karma);
        getLoaderManager().initLoader(LOADER_LICENCE_PLATE, null, this);
        setHasOptionsMenu(true);
        resetUI();
        return root;
    }

    @Override
    public Loader onCreateLoader(int id, Bundle bundle) {
        commentsList.setVisibility(View.GONE);
        emptyComments.setVisibility(View.GONE);
        karma.setVisibility(View.GONE);
        loadCommentsPRB.setVisibility(View.VISIBLE);
        Loader loader = null;
        if (id == LOADER_LICENCE_PLATE) {

            loader = new ReadLicencePlateLoader(getActivity(), strRegion, strStateNumber);
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<List<LicencePlate>> loader, List<LicencePlate> listData) {
        loadCommentsPRB.setVisibility(View.GONE);
        karma.setVisibility(View.VISIBLE);
        if (listData == null) {
            Toast.makeText(getActivity(), R.string.toast_not_internet, Toast.LENGTH_SHORT).show();
            getActivity().finish();
        } else if (listData.size() == 0) {
            emptyComments.setVisibility(View.VISIBLE);
        } else {
            commentsList.setVisibility(View.VISIBLE);
            this.data = listData.get(0);
            adapter = new CommentsAdapter(getActivity(), handler, data.getComments());
        }
        resetUI();
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }

    private void resetUI() {
        stateNumber.setText(String.format(
                        "%s%s",
                        strStateNumber,
                        strRegion
                ).toUpperCase()
        );
        karma.setText(getString(R.string.txt_karma, getCountKarma(data.getComments())));
        commentsList.setAdapter(adapter);
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message message) {
            super.handleMessage(message);

            Boolean status;
            if (message.what == CommentsAdapter.KEY_MSG_ADAPTER &&
                    message.getData().containsKey(CommentsAdapter.KEY_EDIT_KARMA) &&
                    message.getData().containsKey(CommentsAdapter.KEY_POSITION) ) {
                String msg = message.getData().getString(CommentsAdapter.KEY_EDIT_KARMA);
                if (msg.equals(CommentsAdapter.KEY_POSITIVE_KARMA)) {
                    int id = message.getData().getInt(CommentsAdapter.KEY_POSITION);
                    int karma = data.getComments().get(id).getKarma();
                    int id_com = data.getComments().get(id).getId();
                    UpdateCommentKarmaTask task = new UpdateCommentKarmaTask(getActivity(), id_com, 1);
                    task.execute();
                    try {
                        status = task.get();
                        if (status == null) {
                            Toast.makeText(getActivity(), R.string.toast_not_internet, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (status) {
                            Toast.makeText(getActivity(), R.string.toast_add_karma_true, Toast.LENGTH_SHORT).show();
                            data.getComments().get(id).setKarma(karma + 1);
                        } else {
                            Toast.makeText(getActivity(), R.string.toast_add_karma_false, Toast.LENGTH_SHORT).show();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                } else if (msg.equals(CommentsAdapter.KEY_NEGATIVE_KARMA)) {
                    //TODO Изменить
                    int id = message.getData().getInt(CommentsAdapter.KEY_POSITION);
                    int karma = data.getComments().get(id).getKarma();
                    int id_com = data.getComments().get(id).getId();
                    UpdateCommentKarmaTask task = new UpdateCommentKarmaTask(getActivity(), id_com, 0);
                    task.execute();
                    try {
                        status = task.get();
                        if (status == null) {
                            Toast.makeText(getActivity(), R.string.toast_not_internet, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (status) {
                            Toast.makeText(getActivity(), R.string.toast_add_karma_true, Toast.LENGTH_SHORT).show();
                            data.getComments().get(id).setKarma(karma - 1);
                        } else {
                            Toast.makeText(getActivity(), R.string.toast_add_karma_false, Toast.LENGTH_SHORT).show();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                } else {}
            }
            adapter.notifyDataSetChanged();
            resetUI();
        }
    };

    private int getCountKarma(List<Comment> data) {
        int count = 0;
        if (data == null) {
            return count;
        }
        for (Comment c : data) {
            count += c.getKarma();
        }
        return count;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.licence_plat, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                getActivity().onBackPressed();
                break;
            case R.id.action_add_comment:
                onCreateAddComment();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onCreateAddComment() {
        DialogFragment fragment = AddCommentFragment.getInstance(
                new AddCommentFragment.Listener() {
                    @Override
                    public void resetUI(DialogInterface dialog) {
                        dialog.dismiss();
                        loader();
                    }
                },
                strStateNumber,
                strRegion
        );
        fragment.show(getFragmentManager(), AddCommentFragment.TAG);
    }

    private void loader() {
        getLoaderManager().restartLoader(LOADER_LICENCE_PLATE, null, this);
        getLoaderManager().initLoader(LOADER_LICENCE_PLATE, null, this);
    }
}
