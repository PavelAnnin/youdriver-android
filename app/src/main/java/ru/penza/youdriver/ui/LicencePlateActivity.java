package ru.penza.youdriver.ui;

import android.app.Activity;
import android.os.Bundle;

public class LicencePlateActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, LicencePlateFragment.getInstance(), LicencePlateFragment.TAG)
                .commit();
    }

}
