package ru.penza.youdriver.loaders;

import android.content.Context;
import android.os.AsyncTask;
import android.provider.Settings;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pavel on 16.10.2014.
 */
public class UpdateCommentKarmaTask extends AsyncTask<Void, Void, Boolean> {

    private Context context;
    private int id;
    private int karma;

    public UpdateCommentKarmaTask(Context context, int id, int karma) {
        this.context = context;
        this.id = id;
        this.karma = karma;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(Constans.SERVER_URL + "DeviceVoice");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>(3);
            nameValuePairs.add(new BasicNameValuePair("deviceSerialNumber", Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID)));
            nameValuePairs.add(new BasicNameValuePair("karma", String.valueOf(karma)));
            nameValuePairs.add(new BasicNameValuePair("commentId", String.valueOf(id)));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
            HttpResponse response = httpclient.execute(httppost);

            if (response.getStatusLine().getStatusCode() == 500) {
                return false;
            }

        } catch (IOException e) {
            return null;
        }

        return true;
    }

    }
