package ru.penza.youdriver.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ru.penza.youdriver.entities.LicencePlate;

/**
 * Created by pavel on 16.10.2014.
 */
public class ReadLicencePlateLoader extends AsyncTaskLoader<List<LicencePlate>> {

    private String region;
    private String stateNumber;

    public ReadLicencePlateLoader(Context context, String region, String stateNumber ) {
        super(context);
        this.region = region;
        this.stateNumber = stateNumber;
    }

    @Override
    public List<LicencePlate> loadInBackground() {
        List<LicencePlate> data = null;
        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet get = new HttpGet(Constans.SERVER_URL + "findNumber" + "/" + region + "/" + stateNumber);

            HttpResponse response = client.execute(get);

            if(response.getStatusLine().getStatusCode() == 200 ||
                    response.getStatusLine().getStatusCode() == 201) {

                data = new ArrayList<LicencePlate>();
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                Reader reader = new InputStreamReader(content);
                Class<?> entityTypeArray =
                        Class.forName("[L" + LicencePlate.class.getName() + ";");

                data = (List<LicencePlate>) Arrays.asList(new Gson().fromJson(reader, (Class<LicencePlate[]>) entityTypeArray));
            }
        }
        catch(Exception e) {
            return null;
        }
        return data;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }
}
