package ru.penza.youdriver.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pavel on 16.10.2014.
 */
public class AddCommentLoader extends AsyncTaskLoader<Boolean> {

    private String stateNumber;
    private String region;
    private String comment;

    public AddCommentLoader(Context context, String stateNumber, String region, String comment) {
        super(context);
        this.stateNumber = stateNumber;
        this.region = region;
        this.comment = comment;
    }


    @Override
    public Boolean loadInBackground() {

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(Constans.SERVER_URL + "addComment");

        try {

            List<NameValuePair> nameValuePairs = new ArrayList<>(3);
            nameValuePairs.add(new BasicNameValuePair("stateNumber", stateNumber));
            nameValuePairs.add(new BasicNameValuePair("code", region));
            nameValuePairs.add(new BasicNameValuePair("content", comment));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
            HttpResponse response = httpclient.execute(httppost);

        } catch (IOException e) {
            return null;
        }
        return true;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }
}
