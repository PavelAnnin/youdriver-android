package ru.penza.youdriver.fetchers;

import android.content.AsyncTaskLoader;
import android.content.Context;

import ru.penza.youdriver.entities.Entity;

/**
 * Created by MultiNza on 13.10.2014.
 */
public interface Fetcher<E extends Entity> {
    public static final String SERVER_URL = "http://aboutdriver.azurewebsites.net/api";

    public String getUrl();
    public <T extends AsyncTaskLoader> T loader(Context context);
}
