package ru.penza.youdriver.fetchers;

import android.content.Context;

import ru.penza.youdriver.entities.Entity;
import ru.penza.youdriver.fetchers.loaders.CreateLoader;

/**
 * Created by MultiNza on 14.10.2014.
 */
public class CreateFetcher<E extends Entity> implements Fetcher {

    protected E entity;
    protected String action;
    protected Class<E> entityClass;

    public CreateFetcher(E entity, Class<E> entityClass) {
        this.entity = entity;
        this.action = entity.getAction();
        this.entityClass = entityClass;
    }

    @Override
    public String getUrl() {
        return SERVER_URL + "/" + this.action + "/";
    }

    @Override
    public CreateLoader loader(Context context) {
        return new CreateLoader(context, this);
    }

    public Class<E> getEntityClass() {
        return entityClass;
    }

    public E getEntity() {
        return entity;
    }

    public void setEntity(E entity) {
        this.entity = entity;
    }
}
