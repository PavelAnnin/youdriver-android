package ru.penza.youdriver.fetchers.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ru.penza.youdriver.entities.Entity;
import ru.penza.youdriver.entities.Region;
import ru.penza.youdriver.fetchers.ReadFetcher;

/**
 * Created by MultiNza on 13.10.2014.
 */
public class ReadLoader<E extends Entity> extends AsyncTaskLoader<List<E>> {

    protected ReadFetcher fetcher;

    public ReadLoader(Context context, ReadFetcher fetcher) {
        super(context);
        this.fetcher = fetcher;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        this.forceLoad();
    }

    @Override
    public List<E> loadInBackground() {
        List<E> data = null;
        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet get = new HttpGet(this.fetcher.getUrl());

            HttpResponse response = client.execute(get);

            if(response.getStatusLine().getStatusCode() == 200 ||
               response.getStatusLine().getStatusCode() == 201) {

                data = new ArrayList<E>();
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                Reader reader = new InputStreamReader(content);
                Class<?> entityTypeArray =
                        Class.forName("[L" + this.fetcher.getEntityClass().getName() + ";");

                data = (List<E>)Arrays.asList(new Gson().fromJson(reader, (Class<E[]>)entityTypeArray));
                System.out.println();
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        return data;
    }
}
