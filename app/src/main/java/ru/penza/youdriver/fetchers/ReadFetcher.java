package ru.penza.youdriver.fetchers;

import android.content.Context;

import java.lang.reflect.ParameterizedType;
import java.security.interfaces.DSAKey;

import ru.penza.youdriver.entities.Entity;
import ru.penza.youdriver.entities.Region;
import ru.penza.youdriver.fetchers.loaders.ReadLoader;

/**
 * Created by MultiNza on 13.10.2014.
 */
public class ReadFetcher<E extends Entity> implements Fetcher {

    protected String action;
    protected Class<E> entityClass;

    public ReadFetcher(Class<E> entityClass) {
        this.entityClass = entityClass;
        try {
            this.action = entityClass.newInstance().getAction();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getUrl() {
        return SERVER_URL + "/" + this.action + "/";
    }

    @Override
    public ReadLoader loader(Context context) {
        return new ReadLoader(context, this);
    }

    public Class<E> getEntityClass() {
        return entityClass;
    }
}
