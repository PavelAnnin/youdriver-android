package ru.penza.youdriver.fetchers.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import ru.penza.youdriver.entities.Entity;
import ru.penza.youdriver.fetchers.DeleteFetcher;

/**
 * Created by MultiNza on 14.10.2014.
 */
public class DeleteLoader<E extends Entity> extends AsyncTaskLoader<Boolean> {

    protected DeleteFetcher fetcher;
    protected int id;

    public DeleteLoader(Context context, DeleteFetcher fetcher) {
        super(context);
        this.fetcher = fetcher;
        this.id = this.fetcher.getId();
    }

    @Override
    public Boolean loadInBackground() {

        Boolean returnValue = false;

        try {
            HttpClient client = new DefaultHttpClient();
            HttpDelete delete = new HttpDelete(this.fetcher.getUrl());

            HttpParams params = new BasicHttpParams();
            params.setIntParameter("Id", this.id);
            delete.setParams(params);

            HttpResponse response = client.execute(delete);

            if(response.getStatusLine().getStatusCode() == 200 ||
               response.getStatusLine().getStatusCode() == 201) {
               returnValue = true;
            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    return  returnValue;
    }
}
