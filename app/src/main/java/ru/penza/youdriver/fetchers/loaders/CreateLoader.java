package ru.penza.youdriver.fetchers.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.SerializableEntity;
import org.apache.http.impl.client.ClientParamsStack;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.DefaultedHttpParams;
import org.apache.http.params.HttpParams;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Method;

import ru.penza.youdriver.entities.Entity;
import ru.penza.youdriver.fetchers.CreateFetcher;
import ru.penza.youdriver.utils.EntityParse;

/**
 * Created by MultiNza on 14.10.2014.
 */
public class CreateLoader<E extends Entity> extends AsyncTaskLoader<E> {

    protected CreateFetcher fetcher;
    protected E model;

    public CreateLoader(Context context, CreateFetcher fetcher) {
        super(context);
        this.fetcher = fetcher;
        this.model = (E)this.fetcher.getEntity();
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        this.forceLoad();
    }

    @Override
    public E loadInBackground() {

        E returnEntity = null;

        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(this.fetcher.getUrl());

            post.setEntity(
                    new UrlEncodedFormEntity
                            (new EntityParse(this.model).parse())
            );

            HttpResponse response = client.execute(post);

            if(response.getStatusLine().getStatusCode() == 200 ||
               response.getStatusLine().getStatusCode() == 201) {

                HttpEntity receiveEntity =  response.getEntity();
                InputStream content = receiveEntity.getContent();
                Reader reader = new InputStreamReader(content);

                returnEntity = (E)new Gson().fromJson(reader, this.fetcher.getEntityClass());
            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    return  returnEntity;
    }
}
