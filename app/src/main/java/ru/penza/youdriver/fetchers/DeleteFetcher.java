package ru.penza.youdriver.fetchers;

import android.content.Context;

import ru.penza.youdriver.entities.Entity;
import ru.penza.youdriver.fetchers.loaders.DeleteLoader;

/**
 * Created by MultiNza on 14.10.2014.
 */
public class DeleteFetcher<E extends Entity> implements Fetcher {

    protected String action;
    protected Class<E> entityClass;
    protected int id;

    public DeleteFetcher(int id, Class<E> entityClass) {
        this.id = id;
        this.entityClass = entityClass;
        try {
            this.action = entityClass.newInstance().getAction();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getUrl() {
        return SERVER_URL + "/" + this.action + "/";
    }

    @Override
    public DeleteLoader loader(Context context) {
        return new DeleteLoader(context, this);
    }

    public Class<E> getEntityClass() {
        return entityClass;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
