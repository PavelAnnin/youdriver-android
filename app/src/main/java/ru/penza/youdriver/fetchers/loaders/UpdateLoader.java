package ru.penza.youdriver.fetchers.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.SerializableEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import ru.penza.youdriver.entities.Entity;
import ru.penza.youdriver.fetchers.CreateFetcher;
import ru.penza.youdriver.fetchers.UpdateFetcher;
import ru.penza.youdriver.utils.EntityParse;

/**
 * Created by MultiNza on 14.10.2014.
 */
public class UpdateLoader<E extends Entity> extends AsyncTaskLoader<E> {

    protected UpdateFetcher fetcher;
    protected E model;

    public UpdateLoader(Context context, UpdateFetcher fetcher) {
        super(context);
        this.fetcher = fetcher;
        this.model = (E)this.fetcher.getEntity();
    }

    @Override
    public E loadInBackground() {

        E returnEntity = null;

        try {
            HttpClient client = new DefaultHttpClient();
            HttpPut put = new HttpPut(this.fetcher.getUrl());

            put.setEntity(
                    new UrlEncodedFormEntity
                            (new EntityParse(this.model).parse())
            );

            HttpResponse response = client.execute(put);

            if(response.getStatusLine().getStatusCode() == 200 ||
               response.getStatusLine().getStatusCode() == 201) {
                HttpEntity recieveEntity =  response.getEntity();
                InputStream content = recieveEntity.getContent();
                Reader reader = new InputStreamReader(content);

                returnEntity = (E)new Gson().fromJson(reader, this.fetcher.getEntityClass());
            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    return  returnEntity;
    }
}
