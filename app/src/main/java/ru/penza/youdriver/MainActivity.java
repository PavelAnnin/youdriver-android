package ru.penza.youdriver;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import ru.penza.youdriver.ui.ChoiceLicencePlateFragment;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, ChoiceLicencePlateFragment.getInstance(), ChoiceLicencePlateFragment.TAG)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_about:
                onCreateAdout();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onCreateAdout() {
        onCreateAboutDialog().show();
    }

    private AlertDialog onCreateAboutDialog() {
        return new AlertDialog.Builder(MainActivity.this)
                .setTitle(R.string.title_about)
                .setMessage(R.string.msg_about)
                .setNegativeButton(R.string.btn_negative_about,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                .create();
    }

}
