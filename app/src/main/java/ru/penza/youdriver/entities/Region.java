package ru.penza.youdriver.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.penza.youdriver.annotations.FieldParsing;


/**
 * Created by MultiNza on 13.10.2014.
 */
public class Region implements Entity {

    @SerializedName("id")
    private int id;

    @SerializedName("code")
    private int code;

    @SerializedName("name")
    private String name;

    @SerializedName("LicencePlate")
    private List<LicencePlate> licencePlates;

    @Override
    public String getAction() {
        return "Region";
    }

    @FieldParsing
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @FieldParsing
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @FieldParsing
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<LicencePlate> getLicencePlates() {
        return licencePlates;
    }

    public void setLicencePlates(List<LicencePlate> licencePlates) {
        this.licencePlates = licencePlates;
    }
}
