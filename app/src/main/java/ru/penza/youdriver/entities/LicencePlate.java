package ru.penza.youdriver.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.penza.youdriver.annotations.FieldParsing;

/**
 * Created by MultiNza on 13.10.2014.
 */
public class LicencePlate implements Entity {

    @SerializedName("Id")
    private int id;

    @SerializedName("stateNumber")
    private String stateNumber;

    @SerializedName("regionId")
    private int regionId;

    @SerializedName("comments")
    private List<Comment> comments;

    @SerializedName("region")
    private Region region;

    @Override
    public String getAction() {
        return "LicencePlate";
    }

    @FieldParsing
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @FieldParsing
    public String getStateNumber() {
        return stateNumber;
    }

    public void setStateNumber(String stateNumber) {
        this.stateNumber = stateNumber;
    }

    @FieldParsing
    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }
}
