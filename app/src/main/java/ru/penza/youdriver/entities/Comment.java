package ru.penza.youdriver.entities;

import com.google.gson.annotations.SerializedName;

import ru.penza.youdriver.annotations.FieldParsing;

/**
 * Created by MultiNza on 13.10.2014.
 */
public class Comment implements Entity {

    @SerializedName("id")
    public int id;

    @SerializedName("content")
    public String content;

    @SerializedName("licencePlateId")
    public int licencePlateId;

    @SerializedName("licencePlate")
    public LicencePlate licencePlate;

    @SerializedName("karma")
    private int karma;

    @SerializedName("date")
    private String date;

    @Override
    public String getAction() {
        return "Comment";
    }

    @FieldParsing
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @FieldParsing
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @FieldParsing
    public int getLicencePlateId() {
        return licencePlateId;
    }

    public void setLicencePlateId(int licencePlateId) {
        this.licencePlateId = licencePlateId;
    }

    public LicencePlate getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(LicencePlate licencePlate) {
        this.licencePlate = licencePlate;
    }

    public int getKarma() {
        return karma;
    }

    public void setKarma(int karma) {
        this.karma = karma;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
