package ru.penza.youdriver.entities;

import com.google.gson.annotations.SerializedName;

import ru.penza.youdriver.annotations.FieldParsing;

/**
 * Created by MultiNza on 14.10.2014.
 */
public class Device implements Entity {

    @SerializedName("Id")
    private int id;

    @SerializedName("SerialNumber")
    private String serialNumber;

    @Override
    public String getAction() {
        return "Device";
    }

    @FieldParsing
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @FieldParsing
    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}
