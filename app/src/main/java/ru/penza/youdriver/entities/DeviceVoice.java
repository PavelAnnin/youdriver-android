package ru.penza.youdriver.entities;

import com.google.gson.annotations.SerializedName;

import ru.penza.youdriver.annotations.FieldParsing;

/**
 * Created by MultiNza on 14.10.2014.
 */
public class DeviceVoice implements Entity {

    @SerializedName("id")
    private int id;

    @SerializedName("deviceSerialNumber")
    private String deviceSerialNumber;

    @SerializedName("commentid")
    private  int commentId;

    @SerializedName("DeviceVoice")
    private DeviceVoice deviceVoice;

    @SerializedName("karma")
    private int karma;

    @Override
    public String getAction() {
        return "DeviceVoice";
    }

    @FieldParsing
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @FieldParsing
    public String getDeviceSerialNumber() {
        return deviceSerialNumber;
    }

    public void setDeviceSerialNumber(String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }

    @FieldParsing
    public int getKarma() {
        return karma;
    }

    public void setKarma(int karma) {
        this.karma = karma;
    }

    @FieldParsing
    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public DeviceVoice getDeviceVoice() {
        return deviceVoice;
    }

    public void setDeviceVoice(DeviceVoice deviceVoice) {
        this.deviceVoice = deviceVoice;
    }
}
