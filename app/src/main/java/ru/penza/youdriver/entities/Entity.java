package ru.penza.youdriver.entities;

import java.io.Serializable;

import ru.penza.youdriver.annotations.FieldParsing;

/**
 * Created by MultiNza on 13.10.2014.
 */
public interface Entity{
    /**
     * Выводит action сущности для сервера
     *
     * @return
     */
    public String getAction();
}
