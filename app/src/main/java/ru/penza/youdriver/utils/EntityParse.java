package ru.penza.youdriver.utils;

        import org.apache.http.message.BasicNameValuePair;

        import java.lang.reflect.Method;
        import java.util.ArrayList;
        import java.util.List;

        import ru.penza.youdriver.annotations.FieldParsing;
        import ru.penza.youdriver.entities.Entity;

/**
 * Created by MultiNza on 14.10.2014.
 */
public class EntityParse {

    protected Entity _entity;
    protected final static String _prefixMethod = "get";
    protected final static Class classAnnotationsForParsing = FieldParsing.class;


    /**
     *
     * @param entity
     */
    public EntityParse(Entity entity) {
        this._entity = entity;
    }

    /**
     *
     * @return
     */
    public List parse() {
        List parameters = new ArrayList();

        Method methods[] = this._entity.getClass().getDeclaredMethods();

        for(Method m: methods) {

            this.parseMethod(m, parameters);
        }

        return parameters;
    }

    /**
     *
     * @param m
     * @param parameters
     */
    protected void parseMethod(Method m, List parameters) {

        String methodName = m.getName();

        try {

            if(methodName.indexOf(_prefixMethod) == 0) {

                if(m.isAnnotationPresent(classAnnotationsForParsing)) {

                    Object methodReturnValue = m.invoke(this._entity, null);
                    parameters.add(
                            new BasicNameValuePair(m.getName().substring(3),
                                    methodReturnValue.toString())
                    );
                }
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}
